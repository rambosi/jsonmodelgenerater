//
//  ViewController.swift
//  JsonToModel
//
//  Created by SI Tai Lung on 10/1/2020.
//  Copyright © 2020 SI Tai Lung. All rights reserved.
//

import Cocoa

enum ModelType {
    case mappable, codable
}

class ViewController: NSViewController {
    
    @IBOutlet var tvInput: NSTextView!
    @IBOutlet var tvOutput: NSTextView!
    @IBOutlet weak var tfModelName: NSTextField!
    
    @IBOutlet weak var ckDynamic: NSButton!
    @IBOutlet weak var ckPrimaryKey: NSButton!
    @IBOutlet weak var segmentInOut: NSSegmentedControl!
    @IBOutlet weak var segmentType: NSSegmentedControl!
    
    var mModelName = ""
    let dynamicStr = "@objc dynamic "
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvInput.delegate = self
    }
    
    override var representedObject: Any? {
        didSet {
            // Update the view, if already loaded.
        }
    }
    
    func makeJsonModel(jsonString: String? = nil) -> String {
        let inputString = jsonString ?? tvInput.string
        let inputData = inputString.data(using: .utf8)!
        
        guard let json = try? JSONSerialization.jsonObject(
            with: inputData,
            options: JSONSerialization.ReadingOptions.mutableContainers
            ) else {
                return "fails..."
        }
        
        print("\(json)")
        
        var content: String = ""
        makeJsonModelHeader(content: &content, type: getType())
        
        if let jsonObj = json as? [String: Any] {
            makeJsonModelString(fromJsonObject: jsonObj, modelName: tfModelName.stringValue, content: &content)
        } else if let jsonArr = json as? [[String: Any]] {
            guard jsonArr.count > 0 else { return "Json Array empty..." }
            makeJsonModelString(fromJsonObject: jsonArr[0], modelName: tfModelName.stringValue, content: &content)
        }
        
        makeJsonModelTailer(content: &content)
        return content
    }
    
    func makeJsonModelString(fromJsonObject jsonObj: [String:Any], modelName: String, content: inout String, indentation: Int = 0, isSubClass: Bool = false) {
        
        let isInside = segmentInOut.selectedSegment == 0
        let indent = isInside ? indentation : 0
        var define = "", mapping = "", coding = "", defineType = "", codingDecoder = "\n\("\t"*indent)\t\tlet values = try decoder.container(keyedBy: CodingKeys.self)"
        var objects: [String:[String:Any]] = [:]
        let modelType = self.getType()
        let isDynamic = ckDynamic.state == .on
        let isPK = ckPrimaryKey.state == .on
        
        switch modelType {
        case .mappable:
            content += "\n\("\t" * indent)class \(modelName): BaseModel {"
            defineType = isDynamic ? "@objc dynamic var" : "var"
        case .codable:
            content += "\n\("\t" * indent)struct \(modelName): Codable {"
            defineType = "let"
        }
        
        for (key, value) in jsonObj {
            var mType = ""
            if let num = value as? NSNumber {
                switch CFGetTypeID(num as CFTypeRef) {
                case CFBooleanGetTypeID():
                    mType = "Bool?"
                case CFNumberGetTypeID():
                    switch CFNumberGetType(num as CFNumber) {
                    case .sInt8Type, .sInt16Type, .sInt32Type, .sInt64Type:
                        mType = "Int?"
                    case .doubleType:
                        mType = "Double?"
                    default:
                        mType = ""
                    }
                default:
                    mType = ""
                }
            } else if let value = value as? [String:Any] {
                mType = "\(key.capitalizingFirstLetter())Model?"
                objects[key] = value
            } else if let value = value as? [[String:Any]], value.count > 0{
                mType = modelType == .mappable ? "[\(key.capitalizingFirstLetter())Model] = []" : "[\(key.capitalizingFirstLetter())Model]?"
                objects[key] = value[0]
            } else if value is String {
                let str = value as! String
                if str.lowercased() == "true" || str.lowercased() == "false" {
                    mType = "Bool?"
                } else {
                    mType = "String?"
                }
            } else {
                // For null
                mType = "String?"
            }
            
            // let/var XXX: <Type>?
            define += "\n\("\t"*indent)\t\(defineType) \(key.removeLine()): \(mType)"
            // XXX <- map["XXX"]
            mapping += "\n\("\t"*indent)\t\t\(key.removeLine()) <- map[\"\(key)\"]"
            // case XXX = "XXX"
            coding += "\n\("\t"*indent)\t\tcase \(key.removeLine()) = \"\(key)\""
            // XXX = try values.decodeIfPresent(String.self, forKey: .XXX)
            codingDecoder += "\n\("\t"*indent)\t\t\(key.removeLine()) = try values.decodeIfPresent(String.self, forKey: .\(key.removeLine()))"
        }
        
        content += define
        switch modelType {
        case .mappable:
            content += "\n\n\("\t"*indent)\toverride func mapping(map: Map) {\(mapping)\n\("\t"*indent)\t}\n"
            if isPK && !isSubClass {
                content += "\n\("\t"*indent)\toverride class func primaryKey() -> String? {\n\("\t"*indent)\t\treturn \"\"\n\("\t"*indent)\t}\n"
            }
        case .codable:
            content += "\n\n\("\t"*indent)\tenum CodingKeys: String, CodingKey {\(coding)\n\("\t"*indent)\t}\n"
            // decoder
            content += "\n\("\t"*indent)\tinit(from decoder: Decoder) throws {\(codingDecoder)\n\("\t"*indent)\t}\n"
        }
        
        var subModelContent = ""
        for (key, obj) in objects {
            makeJsonModelString(fromJsonObject: obj, modelName: "\(key.capitalizingFirstLetter())Model", content: &subModelContent, indentation: indent+1, isSubClass: true)
        }
        
        if segmentInOut.selectedSegment == 0 {
            content += subModelContent
            content += "\("\t"*indent)}\n"
        } else {
            content += "\("\t"*indent)}\n"
            content += subModelContent
        }
        
    }
    
    // For Header
    func makeJsonModelHeader(content: inout String, type: ModelType) {
        switch type {
        case .mappable:
            content += "import ObjectMapper\n"
        case .codable:
            content += "import Foundation\n"
        }
    }
    
    func makeJsonModelTailer(content: inout String) {
        //        content += "}"
    }
    
    func getType() -> ModelType {
        let types: [ModelType] = [.mappable, .codable]
        return types[self.segmentType.selectedSegment]
    }
    
    @IBAction func ReloadWhenUITap(_ sender: Any) {
        let isMappable = getType() == .mappable
        ckDynamic.isEnabled = isMappable
        ckPrimaryKey.isEnabled = isMappable
        tvOutput.string = makeJsonModel()
    }
    
    @IBAction func tfModelNameChange(_ sender: NSTextField) {
        if mModelName != sender.stringValue {
            print("tfModelNameChange! \(sender.stringValue)")
            mModelName = sender.stringValue
            tvOutput.string = makeJsonModel()
        }
    }
}

extension ViewController: NSTextViewDelegate {
    func textDidChange(_ notification: Notification) {
        if tvInput.string.isEmpty {
            return
        }
        tvOutput.string = makeJsonModel()
    }
}
