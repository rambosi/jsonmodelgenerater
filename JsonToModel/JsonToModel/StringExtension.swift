//
//  StringExtension.swift
//  JsonToModel
//
//  Created by SI Tai Lung on 10/1/2020.
//  Copyright © 2020 SI Tai Lung. All rights reserved.
//

import Cocoa
import Foundation

protocol StringOperatorOverloading {
}

extension String: StringOperatorOverloading {

    static func *(lhs: String, rhs: Int) -> String {
        if rhs <= 0 {
            return ""
        }
        
        var result = ""
        
        for _ in 1...rhs {
            result = result + lhs
        }
        
        return result
    }
    
    // hi_baby ---> hiBaby
    func removeLine() -> String {
        var chars = Array(self)
        
        for i in 0..<self.count {
            let index = self.index(self.startIndex, offsetBy: i)
            if self[index] == "-" || self[index] == "_" {
                let nextIndex = self.index(self.startIndex, offsetBy: i + 1)
                let upperChar = Character(self[nextIndex].uppercased())
                chars[i + 1] = upperChar
            }
        }
        return String(chars)
            .replacingOccurrences(of: "-", with: "")
            .replacingOccurrences(of: "_", with: "")
    }
    
    func capitalizingFirstLetter() -> String {
        return prefix(1).uppercased() + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
