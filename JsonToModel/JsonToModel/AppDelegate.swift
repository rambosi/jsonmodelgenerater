//
//  AppDelegate.swift
//  JsonToModel
//
//  Created by SI Tai Lung on 10/1/2020.
//  Copyright © 2020 SI Tai Lung. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

